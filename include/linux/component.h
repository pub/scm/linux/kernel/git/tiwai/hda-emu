#ifndef _LINUX_COMPONENT_H
#define _LINUX_COMPONENT_H

struct device;

struct component_ops {
	int (*bind)(struct device *comp, struct device *master,
		    void *master_data);
	void (*unbind)(struct device *comp, struct device *master,
		       void *master_data);
};

static inline int component_add(struct device *dev, const struct component_ops *ops) { return -ENODEV; }
static inline void component_del(struct device *dev, const struct component_ops *ops) {}

static inline int component_bind_all(struct device *master, void *master_data) { return -ENODEV; }
static inline void component_unbind_all(struct device *master, void *master_data) {}

struct master;

struct component_master_ops {
	int (*bind)(struct device *master);
	void (*unbind)(struct device *master);
};

static inline void component_master_del(struct device *dev, const struct component_master_ops *ops) {}

struct component_match;

static inline int component_master_add_with_match(struct device *dev,
						  const struct component_master_ops *ops, struct component_match *match) { return -ENODEV; }
static inline void component_match_add_release(struct device *master,
					       struct component_match **matchptr,
					       void (*release)(struct device *, void *),
					       int (*compare)(struct device *, void *), void *compare_data) {}
static inline void component_match_add_typed(struct device *master,
					     struct component_match **matchptr,
					     int (*compare_typed)(struct device *, int, void *), void *compare_data) {}

static inline void component_match_add(struct device *master,
	struct component_match **matchptr,
	int (*compare)(struct device *, void *), void *compare_data)
{
	component_match_add_release(master, matchptr, NULL, compare,
				    compare_data);
}

static inline int component_compare_dev_name(struct device *dev, void *p) { return 0; }
#endif
