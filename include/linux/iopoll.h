#ifndef _LINUX_IOPOLL_H
#define _LINUX_IOPOLL_H

#define readb_poll_timeout(addr, val, cond, delay_us, timeout_us) ({0;})
#define readw_poll_timeout(addr, val, cond, delay_us, timeout_us) ({0;})
#define readl_poll_timeout(addr, val, cond, delay_us, timeout_us) ({0;})

#define read_poll_timeout(op, val, cond, sleep_us, timeout_us, \
			  sleep_before_read, args...) ({0;})
#define read_poll_timeout_atomic(op, val, cond, delay_us, timeout_us, \
				 delay_before_read, args...) ({0;})

#endif

