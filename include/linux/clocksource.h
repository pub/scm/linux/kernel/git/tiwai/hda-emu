#ifndef _LINUX_CLOCKSOURCE_H
#define _LINUX_CLOCKSOURCE_H

#define CLOCKSOURCE_MASK(bits) (cycle_t)((bits) < 64 ? ((1ULL<<(bits))-1) : -1)

#define NSEC_PER_SEC	1000000000

#define clocks_calc_mult_shift(a, b, c, d, e) /* XXX */

#endif
