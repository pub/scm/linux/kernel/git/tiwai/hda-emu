#ifndef _LINUX_KTIME_H
#define _LINUX_KTIME_H

typedef s64 ktime_t;
#define NSEC_PER_SEC      1000000000L
#define NSEC_PER_USEC     1000L
#define ktime_add_ns(kt, nsval)         ((kt) + (nsval))

static inline ktime_t ktime_add_us(const ktime_t kt, const u64 usec)
{
        return ktime_add_ns(kt, usec * NSEC_PER_USEC);
}

static inline int ktime_compare(const ktime_t cmp1, const ktime_t cmp2)
{
        if (cmp1 < cmp2)
                return -1;
        if (cmp1 > cmp2)
                return 1;
        return 0;
}

#endif
