#ifndef _LINUX_REFCOUNT_H
#define _LINUX_REFCOUNT_H

#include "atomic.h"

#define refcount_t atomic_t

#define REFCOUNT_INIT(n)	ATOMIC_INIT(n)
#define refcount_set(r, n)	atomic_set((r), (n))
#define refcount_read(r)	atomic_read(r)
#define refcount_inc(r)		atomic_inc(r)
#define refcount_dec(r)		atomic_dec(r)

static inline bool refcount_dec_and_test(refcount_t *r)
{
	refcount_dec(r);
	return !refcount_read(r);
}
#endif
